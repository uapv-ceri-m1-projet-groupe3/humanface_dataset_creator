# example of preparing the horses and zebra dataset
from os import listdir
from numpy import asarray
from numpy import vstack
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from numpy import savez_compressed
from PIL import Image

# load all images in a directory into memory
def load_images(path, size=(256,256)):
	data_list = list()
	# enumerate filenames in directory, assume all are images
	for filename in listdir(path):
		# load and resize the image
		try:
			pixels = load_img(path + filename, target_size=size)
		# convert to numpy array
		except:
			continue
		pixels = img_to_array(pixels)
		# store
		data_list.append(pixels)
	return asarray(data_list)

# dataset path
path = 'horse2zebra/'
# load dataset A
dataA1 = load_images(path + 'trainA/')
dataAB = load_images(path + 'testA/')
dataA = vstack((dataA1, dataAB))
print('Loaded dataA: ', dataA.shape)
# load dataset B
dataB1 = load_images(path + 'trainB/')
dataB2 = load_images(path + 'testB/')
dataB = vstack((dataB1, dataB2))
print('Loaded dataB: ', dataB.shape)
# save as compressed numpy array
filename = 'simpsonize_new.npz'
savez_compressed(filename, dataA, dataB)
print('Saved dataset: ', filename)
# vocab_size, embedding_size = w2v_weights.shape
# print("Vocabulary Size: {} - Embedding Dim: {}".format(vocab_size, embedding_size))
# print("Vocabulary Size: {} - Embedding Dim: {}".format(vocab_size, embedding_size))

# # Some validation on the quality of the Word2Vec model
# print(w2v_model.wv.most_similar('1', topn=3))
# print(w2v_model.wv.most_similar('good', topn=3))
# print(w2v_model.wv.most_similar('bad', topn=3))
# print(w2v_model.wv.most_similar('4', topn=3))
# print(w2v_model.wv.most_similar(positive=['good', 'great'], negative=['bad'], topn=3))

# categories, ccount = np.unique(DATA['Rating'], return_counts=True)
# plt.figure(figsize=(128, 8))
# plt.title("Dataset Category Distribution")
# plt.bar(categories, ccount, align='center')
# plt.show()